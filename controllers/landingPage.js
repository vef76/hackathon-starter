
const request = require('request-promise');

/**
 * GET /landingPage
 * Page1.
 */
exports.index = (req, res) => {
  res.render('landingPage1', {
    title: 'Landing Page 1'
  });
};

/**
 * GET /landingPage/:id
 * get Page by id
 */
exports.getPage = (req, res) => {
  const id = this.getId(req);

  res.render('landingPage' + id, {
    title: 'Landing Page ' + id
  });
}

exports.getId = (req) => {
    var id = parseInt(req.params.id);
    if (id > 3) {
      id = 2;
    }
    return id;
}


/**
 * POST /landingPage
 * Send new Email to getResponse.com
 */
exports.postContact = (req, res) => {
  const id = this.getId(req);

  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('campaign_token', 'campaign_token').notEmpty();
  req.assert('_csrf', '_csrf').notEmpty();

  const options = {
      method: 'POST',
      uri: 'https://api.getresponse.com/v3/contacts',
      body: {
          "name": req.body.name,
          "email": req.body.email,
          "dayOfCycle": "10",
          "campaign": {
              "campaignId": "Tr7N1"
          },
          "ipAddress": req.connection.remoteAddress.replace(/^.*:/, '')
      },
      json: true,
      headers: {
          'Content-Type': 'application/json',
          'X-Auth-Token': 'api-key 5cc4dcb4d3f484d6bc487ea879392a56'
      }
  }

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/landingPage/' + id);
  }

  request(options).then(function (response){
      console.log(response);
      req.flash('success', { msg: 'Your E-Mail was added successfully. You will Receive an E-Mail for validation!' });
      res.redirect('/landingPage/3');

  })
  .catch(function (err) {
      console.log('Error:' + err);
      req.flash('errors', { msg: err.error.message });
      res.render('landingPage' + id, {
        title: 'Landing Page' + id
      });
  })
}
